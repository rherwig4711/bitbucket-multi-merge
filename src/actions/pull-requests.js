export const UPDATE_SOURCE_BRANCH = 'UPDATE_SOURCE_BRANCH';
export const UPDATE_DESTINATION_BRANCH = 'UPDATE_DESTINATION_BRANCH';
export const UPDATE_TITLE = 'UPDATE_TITLE';
export const UPDATE_DESCRIPTION = 'UPDATE_DESCRIPTION';
export const CREATE_PULL_REQUESTS = 'CREATE_PULL_REQUESTS';
export const CREATE_PULL_REQUESTS_SUCCESS = 'CREATE_PULL_REQUESTS_SUCCESS';
export const CREATE_PULL_REQUESTS_ERROR = 'CREATE_PULL_REQUESTS_ERROR';

import { createPullRequest as create } from '../core/api';

export const updateSourceBranch = (sourceBranchName) => ({
    type: UPDATE_SOURCE_BRANCH,
    payload: {
        sourceBranchName
    }
});

export const updateDestinationBranch = (destinationBranchName, index) => ({
    type: UPDATE_DESTINATION_BRANCH,
    payload: {
        destinationBranchName,
        index
    }
});

export const updateTitle = (title) => ({
    type: UPDATE_TITLE,
    payload: {
        title
    }
});

export const updateDescription = (description) => ({
    type: UPDATE_DESCRIPTION,
    payload: {
        description
    }
});

export const createPullRequests = (pullRequests) => dispatch => {
    const _pullRequests = pullRequests.destinations.filter(destination => !!destination).map(destination => ({
        title: pullRequests.title,
        description: pullRequests.title,
        source: {
            branch: {
                name: pullRequests.source
            }
        },
        destination: {
            branch: {
                name: destination
            }
        }
    }));

    const requests = _pullRequests.map(create);
    Promise.all(requests).then(() => {
        AJS.messages.success({
            title: 'Pull requests created.',
            body: '<p>The pull requests have been created successfully.</p>'
        });

        return dispatch({
            type: CREATE_PULL_REQUESTS_SUCCESS
        });
    }).catch(error => {
        AJS.messages.error({
            title: 'Pull requests could not be created.',
            body: '<p>Please make sure to fill out all required fields.</p>'
        });

        return dispatch({
            type: CREATE_PULL_REQUESTS_ERROR,
            payload: {
                error
            }
        });
    });

    return dispatch({
        type: CREATE_PULL_REQUESTS,
        payload: {
            pullRequests: _pullRequests
        }
    });
};
