import { fetchRepository as fetch } from '../core/api';

export const UPDATE_REPOSITORY = 'UPDATE_REPOSITORY';

export const fetchRepository = () => dispatch => {
    return fetch().then(repository => dispatch(updateRepository(repository)));
};

export const updateRepository = (repository) => ({
    type: UPDATE_REPOSITORY,
    payload: {
        repository
    }
});
