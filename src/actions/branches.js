import { fetchBranches as fetch } from '../core/api';

export const UPDATE_BRANCHES = 'UPDATE_BRANCHES';

export const fetchBranches = (repository) => dispatch => {
    return fetch(repository.full_name).then(branches => dispatch(updateBranches(branches)));
};

export const updateBranches = (branches) => ({
    type: UPDATE_BRANCHES,
    payload: {
        branches
    }
});
