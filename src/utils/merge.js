export default function merge(obj, prop, value) {
    const _obj = {...obj};
    _obj[prop] = value;

    return _obj;
}