export const fetchBranches = (repositoryName) => new Promise((resolve, reject) => {
    AP.require('request', (request) => {
        request({
            url: `/2.0/repositories/${repositoryName}/refs/branches`,
            success (data) {
                return resolve(data.values.map(branch => branch.name));
            },
            error (error) {
                return reject(error);
            }
        });
    });
});

export const createPullRequest = (pullRequest) => new Promise((resolve, reject) => {
    AP.require('request', (request) => {
        request({
            url: '/2.0/repositories/rherwig4711/mconcord/pullrequests',
            type: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            contentType: 'application/json',
            data: JSON.stringify(pullRequest),
            success(data) {
                return resolve(data);
            },
            error(error) {
                return reject(error);
            }
        });
    });
});

export const fetchRepository = () => new Promise((resolve, reject) => {
    const url = new URL(location.href);
    const uuid = url.searchParams.get('repo_uuid');

    AP.require('request', (request) => {
        request({
            url: `/2.0/repositories/{}/${uuid}`,
            success(data) {
                return resolve(data);
            },
            error(error) {
                return reject(error);
            }
        });
    });
});
