export const fetchBranches = () => new Promise((resolve, reject) => {
    return resolve(['development', 'staging', 'production', 'feature/MC-0001']);
});
