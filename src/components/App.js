import React, { Component } from 'react';

import Header from './header/Header';
import MultiMergeContainer from '../containers/MultiMergeContainer';

export default class App extends Component {

    render() {
        return (
            <div className="mm-app">
                <Header/>
                <MultiMergeContainer/>
                <div id="aui-message-bar"/>
            </div>
        );
    }

}
