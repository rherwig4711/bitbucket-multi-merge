import React from 'react';
import PropTypes from 'prop-types';

import './pr-form.scss';

import TextField from '../ui/TextField';
import TextArea from '../ui/TextArea';

const PrForm = ({
    title,
    description,
    onTitleChange,
    onDescriptionChange
}) => (
    <form className="aui top-label mm-input-group">
        <div className="fieldset top-label">
            <TextField
                id="pr-title"
                label="Title"
                value={title}
                onChange={onTitleChange}/>
            <TextArea
                id="pr-description"
                label="Description"
                value={description}
                onChange={onDescriptionChange}/>
        </div>
    </form>
);

PrForm.propTypes = {
    title: PropTypes.string.isRequired,
    description: PropTypes.string.isRequired,
    onTitleChange: PropTypes.func.isRequired,
    onDescriptionChange: PropTypes.func.isRequired
};

export default PrForm;
