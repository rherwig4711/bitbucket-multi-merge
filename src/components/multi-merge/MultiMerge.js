import React from 'react';
import PropTypes from 'prop-types';

import './multi-merge.scss';

import Dropdown from '../ui/Dropdown';
import PrForm from '../pr-form/PrForm';
import Button from '../ui/Button';

const MultiMerge = ({
    branches,
    pullRequests,
    onSourceChange,
    onDestinationChange,
    onTitleChange,
    onDescriptionChange,
    onCreateClick
}) => (
    <main className="multi-merge">
        <section className="mm-wrapper">
            <section className="mm-branches">
                <div className="aui-message mm-message-branch">
                    <p className="title">
                        <strong>Source</strong>
                    </p>
                    <p>Choose, which branch you want to merge.</p>

                    <Dropdown
                        id="source-dropdown"
                        onChange={onSourceChange}
                        value={pullRequests['source'] || 'Choose a branch...'}
                        items={branches}/>
                </div>
                <div className="arrow-container">
           <span className="aui-icon aui-icon-small aui-iconfont-devtools-compare arrow">
                Arrow
            </span>
                </div>
                <div className="aui-message mm-message-branch">
                    <p className="title">
                        <strong>Destination</strong>
                    </p>
                    <p>
                        Choose one or more branches to merge to.
                    </p>
                    {pullRequests.destinations.map((destination, index) =>
                        <Dropdown
                            key={index}
                            id={`destination-dropdown-${index}`}
                            onChange={onDestinationChange(index)}
                            value={pullRequests['destinations'][index] || 'Choose a branch...'}
                            items={branches}/>
                    )}
                </div>
            </section>
            <section className="mm-form">
                <PrForm
                    title={pullRequests.title}
                    description={pullRequests.description}
                    onTitleChange={onTitleChange}
                    onDescriptionChange={onDescriptionChange}/>
            </section>
        </section>
        <section className="button-section">
            <Button
                label="Create pull requests"
                icon="aui-iconfont-devtools-pull-request"
                onClick={onCreateClick}
                disabled={!isModelValid(pullRequests)}/>
        </section>
    </main>
);

MultiMerge.propTypes = {
    branches: PropTypes.array.isRequired,
    pullRequests: PropTypes.object.isRequired,
    onSourceChange: PropTypes.func.isRequired,
    onDestinationChange: PropTypes.func.isRequired,
    onTitleChange: PropTypes.func.isRequired,
    onDescriptionChange: PropTypes.func.isRequired,
    onCreateClick: PropTypes.func.isRequired
};

const isModelValid = (pullRequests) => {
    return !!pullRequests.title && !!pullRequests.source && pullRequests.destinations.some(x => !!x);
};

export default MultiMerge;
