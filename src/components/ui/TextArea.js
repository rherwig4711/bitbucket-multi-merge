import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TextArea extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired
    };

    handleChange = (event) => {
        this.props.onChange(event.target.value);
    };

    render() {
        const { id, label, value } = this.props;

        return (
            <div className="field-group top-label field-group-textarea">
                <label htmlFor={id}>{label}</label>
                <textarea
                    className="textarea full-width-field"
                    onChange={this.handleChange}
                    value={value}
                    id={id}>
                </textarea>
            </div>
        );
    }
}
