import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Button extends Component {

    static propTypes = {
        label: PropTypes.string.isRequired,
        icon: PropTypes.string,
        disabled: PropTypes.bool,
        onClick: PropTypes.func.isRequired
    };

    handleClick = () => {
        const { onClick, disabled } = this.props;

        if (onClick && !disabled) {
            onClick();
        }
    };

    render() {
        const { label, icon, disabled } = this.props;

        return (
            <button
                onClick={this.handleClick}
                className="aui-button aui-button-primary"
                disabled={disabled}>
                { icon && <span className={`aui-icon aui-icon-small ${icon}`}/>  }
                {label}
            </button>
        );
    }
}
