import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class Dropdown extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        items: PropTypes.array.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func
    };

    handleSelect = (event) => {
        const { onChange } = this.props;

        if (onChange) {
            onChange(event.target.innerHTML);
        }
    };

    handleReset = () => {
        const { onChange } = this.props;

        if (onChange) {
            onChange(null);
        }
    };

    render() {
        const { id, items, value } = this.props;

        return (
            <div className="mm-dropdown">
                <a
                    href={`#${id}-trigger`}
                    aria-owns={id}
                    className="aui-button aui-style-default aui-dropdown2-trigger">
                    {value}
                </a>
                <div id={id} className="aui-style-default aui-dropdown2">
                    <div className="aui-dropdown2-section">
                        <ul>
                            <li><a href="#" onClick={this.handleReset}>None</a></li>
                        </ul>
                    </div>
                    <div className="aui-dropdown2-section">
                        <div className="aui-dropdown2-heading">
                            <strong>Branches</strong>
                        </div>
                        <ul>
                            {items.map((item, index) =>
                                <li key={index}>
                                    <a href="#" onClick={this.handleSelect}>{ item }</a>
                                </li>
                            )}
                        </ul>
                    </div>
                </div>
            </div>
        );
    }

}
