import React, { Component } from 'react';
import PropTypes from 'prop-types';

export default class TextField extends Component {

    static propTypes = {
        id: PropTypes.string.isRequired,
        label: PropTypes.string.isRequired,
        value: PropTypes.string.isRequired,
        onChange: PropTypes.func.isRequired
    };

    handleChange = (event) => {
        this.props.onChange(event.target.value);
    };

    render() {
        const { id, label, value } = this.props;

        return (
            <div className="field-group top-label field-group-textfield">
                <label htmlFor={id}>{label}</label>
                <input
                    value={value}
                    onChange={this.handleChange}
                    className="text full-width-field"
                    type="text"
                    id={id} />
            </div>
        );
    }
}
