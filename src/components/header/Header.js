import React, { Component } from 'react';
import { connect } from 'react-redux';

import './header.scss';

const mapStateToProps = (state) => ({
    repository: state.repository
});

@connect(mapStateToProps)
export default class Header extends Component {

    render() {
        const { repository } = this.props;

        return (
            <header className="mm-header">
                <div className="breadcrumb">{repository.full_name}</div>
                <h1 className="headline">Multi Merge</h1>
                <h2 className="subline">Create pull requests</h2>
            </header>
        );
    }

}
