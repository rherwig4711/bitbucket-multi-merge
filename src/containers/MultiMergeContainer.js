import React, { Component } from 'react';
import { connect } from 'react-redux';

import { fetchBranches} from '../actions/branches';
import { fetchRepository } from '../actions/repository'
import {
    updateSourceBranch,
    updateDestinationBranch,
    updateTitle,
    updateDescription,
    createPullRequests
} from '../actions/pull-requests'

import MultiMerge from '../components/multi-merge/MultiMerge';

const mapStateToProps = (state) => ({
    branches: state.branches,
    pullRequests: state.pullRequests,
    repository: state.repository
});

@connect(mapStateToProps, {
    fetchBranches,
    updateSourceBranch,
    updateDestinationBranch,
    updateTitle,
    updateDescription,
    createPullRequests,
    fetchRepository
})
export default class MultiMergeContainer extends Component {

    componentDidMount() {
        this.props.fetchRepository().then((action) => this.props.fetchBranches(action.payload.repository));
    }

    handleSourceChange = sourceBranchName => {
        this.props.updateSourceBranch(sourceBranchName);
    };

    handleDestinationChange = index => destinationBranchName => {
        this.props.updateDestinationBranch(destinationBranchName, index);
    };

    handleTitleChange = title => {
        this.props.updateTitle(title);
    };

    handleDescriptionChange = description => {
        this.props.updateDescription(description);
    };

    handleCreateClick = () => {
        const { pullRequests, createPullRequests } = this.props;
        createPullRequests(pullRequests);
    };

    render() {
        const { branches, pullRequests } = this.props;

        return (
            <MultiMerge
                branches={branches}
                pullRequests={pullRequests}
                onSourceChange={this.handleSourceChange}
                onDestinationChange={this.handleDestinationChange}
                onTitleChange={this.handleTitleChange}
                onDescriptionChange={this.handleDescriptionChange}
                onCreateClick={this.handleCreateClick}/>
        );
    }

}
