import { combineReducers } from 'redux'

import branches from './branches';
import pullRequests from './pull-requests';
import repository from './repository';

export default combineReducers({
    branches,
    pullRequests,
    repository
});
