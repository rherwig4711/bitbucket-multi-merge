import { UPDATE_REPOSITORY } from '../actions/repository'

export default (state = {}, action) => {
    switch (action.type) {
        case UPDATE_REPOSITORY:
            return action.payload.repository || state;
        default:
            return state;
    }
};
