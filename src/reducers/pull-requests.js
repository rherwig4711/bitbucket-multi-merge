import {
    UPDATE_SOURCE_BRANCH,
    UPDATE_DESTINATION_BRANCH,
    UPDATE_TITLE,
    UPDATE_DESCRIPTION,
    CREATE_PULL_REQUESTS_SUCCESS
} from '../actions/pull-requests';

import merge from '../utils/merge';

const initialState = {
    title: '',
    description: '',
    source: '',
    destinations: [null, null]
};

const defaultTitlePrefix = 'Pull request: ';

const updateSourceBranch = (state, sourceBranchName) => {
    let _state = merge(state, 'source', sourceBranchName);

    if (!_state.title || _state.title === `${defaultTitlePrefix}${state.source}`) {
        _state.title = `${defaultTitlePrefix}${_state.source}`;
    }

    return _state;
};

const updateDestinationBranch = (state, destinationBranchName, index) => {
    const _state = {...state};
    _state.destinations[index] = destinationBranchName;

    const destinationCount = _state.destinations.length;
    const lastIndex = destinationCount - 1;

    if (index === lastIndex && destinationBranchName) {
        _state.destinations.push('');
    } else if (
        index + 1 === lastIndex
        && !destinationBranchName
        && !_state.destinations[lastIndex]
        && destinationCount > 2) {
       _state.destinations.pop();
    }

    return _state;
};

const updateTitle = (state, title) => merge(state, 'title', title);

const updateDescription = (state, description) => merge(state, 'description', description);

export default (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_SOURCE_BRANCH:
            return updateSourceBranch(state, action.payload.sourceBranchName);
        case UPDATE_DESTINATION_BRANCH:
            return updateDestinationBranch(state, action.payload.destinationBranchName, action.payload.index);
        case UPDATE_TITLE:
            return updateTitle(state, action.payload.title);
        case UPDATE_DESCRIPTION:
            return updateDescription(state, action.payload.description);
        case CREATE_PULL_REQUESTS_SUCCESS:
            return initialState;
        default:
            return state;
    }
}
