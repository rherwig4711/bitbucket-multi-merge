import { UPDATE_BRANCHES } from '../actions/branches'

export default (state = [], action) => {
    switch (action.type) {
        case UPDATE_BRANCHES:
            return action.payload.branches || state;
        default:
            return state;
    }
};
