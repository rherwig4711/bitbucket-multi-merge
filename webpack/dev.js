const webpack = require('webpack');
const ExtractTextWebpackPlugin = require('extract-text-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const join = require('path').join;

module.exports = {
    entry: [
        'webpack-dev-server/client?http://0.0.0.0:8080',
        'webpack/hot/only-dev-server',
        join(__dirname, '../src/index')
    ],
    output: {
        path: join(__dirname, '../public'),
        filename: 'multi-merge.js'
    },
    resolve: {
        extensions: ['.js']
    },
    module: {
        rules: [{
            test: /\.js$/,
            loader: 'babel-loader'
        }, {
            test: /\.scss$/,
            use: ExtractTextWebpackPlugin.extract({
                fallback: 'style-loader',
                use: 'css-loader!sass-loader'
            })
        }, {
            test: /\.(eot|svg|ttf|woff|woff2)$/,
            loader: 'file-loader?name=public/fonts/[name].[ext]'
        }]
    },
    devServer: {
        host: '0.0.0.0',
        port: 8080,
        contentBase: 'public/'
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new ExtractTextWebpackPlugin('multi-merge.css'),
        new HtmlWebpackPlugin({
            template: join(__dirname, '../src/index.html')
        })
    ]
};
